# INSTALL
# (1) download packages
#     - wget
# (2) build dotnet core 3.1 projects
#     - dotnet core 3.1 sdk
#     - libicu, lttng-ust, userspace-rcu
#     - zlib-devel, krb5-devel, ncurses-devel, openssl-devel (linux AOT)
#     - centos-release-scl, llvm-toolset-7 (linux AOT requrie Clang >= 3.9)
# (3) run dotnet-sonarscanner
#     - dotnet core 2.2 runtime
#     - which
#     - java-11-openjdk
# (4) git and git LFS support
#     - git
#     - git-lfs
FROM centos:7
ENV DOTNET_SYSTEM_GLOBALIZATION_INVARIANT=false \
    # LC_ALL=en_US.UTF-8 \
    # LANG=en_US.UTF-8 \
    NUGET_XMLDOC_MODE=skip \
    DOTNET_CLI_TELEMETRY_OPTOUT=1 \
    CppCompilerAndLinker=clang \
    PATH="/root/.dotnet/tools:${PATH}"
RUN yum provides '*/applydeltarpm' \
    && yum install -y deltarpm \
    && yum install -y git wget which libicu lttng-ust userspace-rcu zlib-devel krb5-devel ncurses-devel openssl-devel centos-release-scl java-11-openjdk \
    && yum install -y llvm-toolset-7 \
    && rpm -Uvh https://packages.microsoft.com/config/centos/7/packages-microsoft-prod.rpm \
    && yum install -y dotnet-runtime-2.2 \
    # && yum install -y dotnet-sdk-3.1 \
    && yum autoremove -y \
    && rm -rf /var/cache/yum/* \
    ## install dotnet
    && wget -O dotnet.tar.gz https://download.visualstudio.microsoft.com/download/pr/d731f991-8e68-4c7c-8ea0-fad5605b077a/49497b5420eecbd905158d86d738af64/dotnet-sdk-3.1.100-linux-x64.tar.gz \
    && dotnet_sha512='5217ae1441089a71103694be8dd5bb3437680f00e263ad28317665d819a92338a27466e7d7a2b1f6b74367dd314128db345fa8fff6e90d0c966dea7a9a43bd21' \
    && echo "$dotnet_sha512  dotnet.tar.gz" | sha512sum -c - \
    && mkdir -p /usr/share/dotnet \
    && tar -ovxzf dotnet.tar.gz -C /usr/share/dotnet \
    && rm -rf /usr/bin/dotnet \
    && ln -s /usr/share/dotnet/dotnet /usr/bin/dotnet \
    && rm dotnet.tar.gz \
    ## install git-lfs
    && wget -O gitlfs.rpm https://packagecloud.io/github/git-lfs/packages/el/7/git-lfs-2.9.1-1.el7.x86_64.rpm/download \
    && gitlfs_sha512='2b26fc4ede482369f6f8b7f53cbdda2008a37686c90f1d6ca2aa2e012621c0a1ae3b2d3674f985fcdf31c0719e95ed2bb2591264628270c2e2931ddfcd0cc8fd' \
    && echo "$gitlfs_sha512  gitlfs.rpm" | sha512sum -c - \
    && rpm -ivh gitlfs.rpm \
    && git lfs install \
    && rm gitlfs.rpm \
    ## print info
    && dotnet --info \
    && git version \
    && git lfs version \
    && java -version

# if use sonarscanner cli version, yum install unzip and append follow
    # && sonarscanner_ver='4.2.0.1873' \
    # && wget -O sonarscanner.zip https://binaries.sonarsource.com/Distribution/sonar-scanner-cli/sonar-scanner-cli-${sonarscanner_ver}-linux.zip \
    # && sonarscanner_sha512='c70e8b4b5fed0708cff1f671dbaaefeff3d6feb07b8cb3d926286d5bb1285a295d79ef3075c3202ac29c6e2b4dad198dbb7558f7e4301ee26115866202e304fe' \
    # && echo "$sonarscanner_sha512  sonarscanner.zip" | sha512sum -c - \
    # && unzip sonarscanner.zip -d /usr/share \
    # && ln -s /usr/share/sonar-scanner-${sonarscanner_ver}-linux/bin/sonar-scanner /usr/bin/sonar-scanner \
    # && ln -s /usr/share/sonar-scanner-${sonarscanner_ver}-linux/bin/sonar-scanner-debug /usr/bin/sonar-scanner-debug \
    # && rm -rf sonarscanner/ sonarscanner.zip

# you can use -v argument to mount host path
#    docker run -ti --rm -v $PWD:/app m2nlight/dotnet3sdk bash -c 'dotnet build -r osx-x64 -c release'
# or execute follow in container shell (support AOT build):
#    docker run -ti --rm -v $PWD:/app m2nlight/dotnet3sdk
#    chmod +x *.sh
#    ./publish_all.sh -r linux-x64 --vs beta.1
#    exit
#    ls -lh publish/**/*
WORKDIR /app
CMD [ "/usr/bin/scl", "enable", "llvm-toolset-7", "bash" ]
